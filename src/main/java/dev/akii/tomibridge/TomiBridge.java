package dev.akii.tomibridge;

import dev.akii.tomibridge.discord.Lambot;
import dev.akii.tomibridge.minecraft.commands.*;
import dev.akii.tomibridge.minecraft.events.DeathListener;
import dev.akii.tomibridge.minecraft.events.PlayerLoginListener;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Set;

public class TomiBridge extends JavaPlugin {

  private Lambot lambot;
  private Utils.CustomConfig whitelist;
  private Utils.CustomConfig reapedPlayers;
  private Reaper reaper;

  private Guild guild;

  @Override
  public void onEnable() {
    getLogger().info("onEnable called :)");
    getLogger().info("Booting the bot...");

    this.lambot = new Lambot(this);

    // dev.akii.tomibridge.Utils - collection of static methods
    Utils.initConfig(this);

    // Initialize the plugin whitelist linker
    this.whitelist = new Utils.CustomConfig(this, "whitelist.yml");
    Utils.initWhitelist(this);

    this.reapedPlayers = new Utils.CustomConfig(this, "reapedplayers.yml");
    Utils.initReapedPlayers(this);

    // Initialize the death manager
    String guildID = this.getConfig().getString("discord.guildID");
    if (guildID == null) {
      getLogger().severe("config.yml path discord.guildID should not be null");
      return;
    }

    Guild guild = lambot.getAPI().getGuildById(guildID);
    if (guild == null) {
      getLogger().severe("config.yml path discord.guildID was invalid; I couldn't find that guild!");
      getPluginLoader().disablePlugin(this);
      return;
    }
    this.guild = guild;

    String deathRoleID = this.getConfig().getString("discord.deathRoleID");
    if (deathRoleID == null) {
      getLogger().severe("config.yml path discord.deathRoleID should not be null");
      return;
    }

    try {
      this.reaper = new Reaper(this, guildID, deathRoleID);
    } catch (Exception e) {
      e.printStackTrace();
      getPluginLoader().disablePlugin(this);
      return;
    }

    // When the plugin starts up again after a restart, resume the existing death timers
    resumeReaperTimers();

    // Register all the minecraft commands
    initializeMinecraftCommands();

    getLogger().info("Registering listeners...");

    DeathListener deathListener = new DeathListener(this);
    PlayerLoginListener loginListener = new PlayerLoginListener(this);

    getServer().getPluginManager().registerEvents(deathListener, this);
    getServer().getPluginManager().registerEvents(loginListener, this);

    if (getServer().hasWhitelist())
      getLogger().warning("Warning: Whitelist is enabled on this server. This is something that TomiBridge provides as an alternative.");

    getLogger().info("Done");
  }

  @Override
  public void onDisable() {
    getLogger().info("onDisable called :)");

    this.lambot.getAPI().shutdown();
  }

  private void resumeReaperTimers() {
    Reaper reaper = this.getReaper();
    Guild guild = this.guild;

    FileConfiguration config = getReapedPlayers().getConfig();
    Set<String> keys = config.getKeys(false);

    keys.forEach((String ip) -> {
      getLogger().info("Resuming reaper timer for " + ip);

      String until = config.getString(ip + ".until");
      assert until != null;
      String discordID = config.getString(ip + ".discordID");
      assert discordID != null;

      Member member = guild.retrieveMemberById(discordID).complete();

      LocalDateTime nowDate = LocalDateTime.now();
      LocalDateTime untilDate = LocalDateTime.parse(until);

      // ah type casting, comp sci 101 and i still hated it.
      // nowDate.until() returns a long. luckily, the value will never be long enough to cause an overflow
      // if i cast to int.
      int difference = (int) (ChronoUnit.MINUTES.between(nowDate, untilDate));

      this.getLogger().fine("difference: " + difference + " minutes");

      getLogger().fine("member " + member);
      getLogger().fine("ip " + ip);
      getLogger().fine("difference " + difference);

      // Their timer has expired already! Resurrect them.
      if (difference < 0) {
        reaper.resurrect(member, ip);
        return;
      }

      reaper.setTimer(member, ip, difference);
    });
  }

  private void initializeMinecraftCommands() {
    ArrayList<Command> commandList = new ArrayList<>();

    commandList.add(new SetLogger(this));
    commandList.add(new ReloadConfig(this));
    commandList.add(new Resurrect(this));
    commandList.add(new SubOnlyMode(this));
    commandList.add(new About(this));

//    commandList.forEach((command) -> {
//      command.permissionMessage(MCResponse.getPrefix().append(Component.text("You do not have access to this command!", NamedTextColor.RED)));
//    });

    CommandMap commandMap = this.getServer().getCommandMap();
    commandMap.registerAll("tomibridge", commandList);
  }

  public boolean getDebugMode() {
    FileConfiguration config = this.getConfig();

    return config.getBoolean("debugMode");
  }

  public Lambot getBot() {
    return this.lambot;
  }

  public Utils.CustomConfig getWhitelist() {
    return this.whitelist;
  }

  public Utils.CustomConfig getReapedPlayers() {
    return this.reapedPlayers;
  }

  public Reaper getReaper() {
    return this.reaper;
  }

  public Guild getGuild() {
    return this.guild;
  }
}
