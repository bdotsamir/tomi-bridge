package dev.akii.tomibridge.minecraft.events;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.Utils;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.jetbrains.annotations.NotNull;

public class PlayerLoginListener implements Listener {

  private final TomiBridge plugin;

  public PlayerLoginListener(@NotNull TomiBridge plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onPlayerJoin(PlayerLoginEvent event) {
    String uuid = event.getPlayer().getUniqueId().toString();

    Utils.CustomConfig whitelist = plugin.getWhitelist();

    String uuidInWhitelist = whitelist.getConfig().getString(uuid);

    TextComponent kickMessage = Component.text("You have not yet linked your Discord account!", NamedTextColor.RED)
            .decoration(TextDecoration.BOLD, false)
            .append(Component.text("\n\n"))
            .append(Component.text("Please run\n", NamedTextColor.WHITE))
            .append(Component.text(">whitelist " + event.getPlayer().getName(), NamedTextColor.AQUA, TextDecoration.BOLD))
            .append(Component.text("\nin the #mc-whitelist channel.", NamedTextColor.WHITE));

    // Kick the player because they aren't whitelisted.
    if(uuidInWhitelist == null) {
      event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, kickMessage);
      return;
    }

    String guildID = plugin.getConfig().getString("discord.guildID");
    assert guildID != null;

    String discordID = whitelist.getConfig().getString(uuid + ".discordID");
    if(discordID == null) {
      plugin.getLogger().severe("whitelist.yml " + uuid + ".discordID was null.");
      return;
    }

    Guild guild = plugin.getGuild();
    Member member = guild.retrieveMemberById(discordID).complete();
    // There was a problem getting the Discord information for this UUID. Kick them for it.
    if(member == null) {
      TextComponent memberMismatchError = Component.text("There was a problem retrieving your Discord account.", NamedTextColor.RED)
              .append(Component.text("\nAsk an admin to remove you from the whitelist so you can re-whitelist yourself.", NamedTextColor.WHITE));

      plugin.getLogger().severe("User joining the server " + event.getPlayer().getName() + " did not correspond to a member on Discord, despite their ID being on the whitelist. Perhaps they have a new account?");
      event.disallow(PlayerLoginEvent.Result.KICK_OTHER, memberMismatchError);
      return;
    }

    String memberType = whitelist.getConfig().getString(uuid + ".type");
    assert memberType != null;

    plugin.getLogger().fine("member type: " + memberType);

    boolean isSubModeEnabled = plugin.getConfig().getBoolean("subOnlyMode");
    plugin.getLogger().fine("is sub mode enabled according to the config? " + isSubModeEnabled);

    if(!isSubModeEnabled) {
      plugin.getLogger().info("Cancelled checking for user " + event.getPlayer().getName() + "'s sub status because sub-only mode is disabled.");
      return;
    }

    // If the user has been nepotism'd, allow them through without checking whether they're subbed to tomi.
    if(memberType.equals("nepotism")) {
      plugin.getLogger().fine("member was allowed into the server because of nepotism.");
      event.allow();
      return;
    // else if they're a subscriber...
    } else if(memberType.equals("subscriber")) {
      // get the subscriber role ID on discord
      String subRoleID = plugin.getConfig().getString("discord.subRoleID");
      assert subRoleID != null; // because at this poit it *will* be available.

      Role subRole = guild.getRoleById(subRoleID);
      // If there was a problem getting the subscriber role,
      if(subRole == null) {
        plugin.getLogger().severe("Invalid subscriber role ID. Role could not be found on Discord.");

        TextComponent roleNotFoundError = Component.text("There was a problem finding the Twitch subscriber role on Discord, so unfortunately you cannot join :( Sorry.", NamedTextColor.RED)
                .append(Component.text("\nPlease notify an admin on Discord about this so that they can fix it!"));

        // disallow them.
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, roleNotFoundError);
        return;
      }

      // if the user doesn't have the subscriber role,
      if(!member.getRoles().contains(subRole)) {
        TextComponent memberNotSubscribed = Component.text("You are no longer subscribed to scrungeonmaster on Twitch!", NamedTextColor.RED)
                .append(Component.text("\nYou'll need to resubscribe to them in order to access this server again.", NamedTextColor.WHITE));

        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, memberNotSubscribed);
        return;
      } else {
        event.allow();
      }
    } else {
      String errorMessage = "Invalid member type. Received: " + memberType + ". Expected one of: [nepotism, subscriber]";
      plugin.getLogger().severe(errorMessage);
      event.disallow(PlayerLoginEvent.Result.KICK_OTHER, Component.text(errorMessage, NamedTextColor.RED));
      return;
    }

    plugin.getLogger().info("Allowed user " + event.getPlayer().getName() + " into the server.");
  }

}
