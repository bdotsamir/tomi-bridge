package dev.akii.tomibridge.minecraft.events;

import dev.akii.tomibridge.TomiBridge;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.jetbrains.annotations.NotNull;

public class DeathListener implements Listener {

  //private final Lambot lambot;
  private final TomiBridge plugin;
  private final JDA api;

  // Dependency injection!
  // TomiBridge is the name of the plugin, it extends JavaPlugin.
  // TomiBridge has the added method getBot() which returns the Lambot instance created at startup.
  // From there, we can then use Lambot's getAPI() method to do all the things:tm: we need to on Discord.
  public DeathListener(@NotNull TomiBridge plugin) {
    //this.lambot = lambot;
    this.plugin = plugin;
    this.api = plugin.getBot().getAPI();
  }

  @EventHandler
  public void onPlayerDeath(@NotNull PlayerDeathEvent event) {
    // If the player died to something other than PvP, don't continue.
    if(event.getEntity().getKiller() == null)
      return;

    String playerName = PlainTextComponentSerializer.plainText().serialize(event.getEntity().displayName());

    String cum = "Uh oh, " + playerName + " died. ";

    plugin.getLogger().info(cum);

    try {
      String guildID = plugin.getConfig().getString("discord.guildID");
      if(guildID == null) {
        plugin.getLogger().severe("config.yml path discord.guildID should not be null");
        return;
      }
      Guild guild = plugin.getGuild();

      String channelID = plugin.getConfig().getString("discord.channelID");
      if(channelID == null) {
        plugin.getLogger().severe("config.yml path discord.channelID should not be null");
        return;
      }
      TextChannel channel = guild.getTextChannelById(channelID);
      if(channel == null) {
        plugin.getLogger().severe("config.yml path discord.channelID could not be found on Discord");
        return;
      }

      channel.sendMessage(cum).queue();

      //String formattedUUID = Utils.formatUUID(event.getEntity().getUniqueId().toString());

      plugin.getLogger().info("uuid: " + event.getEntity().getUniqueId());
      String discordUserID = plugin.getWhitelist().getConfig().getString(event.getEntity().getUniqueId() + ".discordID");
      assert discordUserID != null;
      Member deadMember = guild.retrieveMemberById(discordUserID).complete();
      plugin.getLogger().info("member name: " + deadMember.getUser().getName());

      plugin.getReaper().reap(deadMember, event.getEntity());

    } catch (Exception e) {
      e.printStackTrace();
      Bukkit.getLogger().severe("Channel to send death message to does not exist");
    }

  }

}
