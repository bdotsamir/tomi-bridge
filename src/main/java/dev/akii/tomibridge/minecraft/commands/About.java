package dev.akii.tomibridge.minecraft.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.minecraft.CompositeCommand;
import dev.akii.tomibridge.minecraft.MCResponse;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class About extends CompositeCommand {

  private final TomiBridge plugin;

  public About(TomiBridge plugin) {
    super("about", "About the plugin", "/<command>", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("tomibridge.about");
    this.setPermissionRequired(false);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, String[] args) {
    Component message = MCResponse.getPrefix()
            .append(Component.text("TomiBridge", NamedTextColor.RED)
                    .append(Component.text(" - ", NamedTextColor.GRAY))
                    .append(Component.text("Version: v" + plugin.getDescription().getVersion() + "\n", NamedTextColor.WHITE)))
            .append(Component.text(" - ", NamedTextColor.GRAY))
            .append(Component.text("Created by:", NamedTextColor.WHITE))
            .append(Component.text(" Strangnessness ", NamedTextColor.AQUA))
            .append(Component.text("(@Strange#0009)\n", NamedTextColor.WHITE))
            .append(Component.text(" - ", NamedTextColor.GRAY))
            .append(Component.text("Connects Tomi's Discord and this server.\n", NamedTextColor.WHITE))
            .append(Component.text(" - ", NamedTextColor.GRAY))
            .append(Component.text("View the source here: ", NamedTextColor.WHITE)
                    .append(Component.text("https://gitlab.com/akii0008/tomi-bridge", NamedTextColor.GOLD)
                            .clickEvent(ClickEvent.openUrl("https://gitlab.com/akii0008/tomi-bridge"))));

    sender.sendMessage(message);
    return true;
  }

}
