package dev.akii.tomibridge.minecraft.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.minecraft.CompositeCommand;
import dev.akii.tomibridge.minecraft.MCResponse;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SubOnlyMode extends CompositeCommand {

  TomiBridge plugin;

  public SubOnlyMode(TomiBridge plugin) {
    super("subonly", "Enables/disables/toggles sub-only mode for this server", "<command> [true/false]", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("tomibridge.subonly");
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    boolean subOnlyMode = plugin.getConfig().getBoolean("subOnlyMode");

    try {
      String enabledDisabled = args[0];
      if (enabledDisabled.equals("true"))
        subOnlyMode = true;
      else if (enabledDisabled.equals("false")) {
        subOnlyMode = false;
      } else {
        sender.sendMessage(MCResponse.error("Must be one of \"true\" or \"false\" (or omitted to toggle it)."));
        return false;
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      subOnlyMode = !subOnlyMode; // toggle the boolean
    }

    plugin.getConfig().set("subOnlyMode", subOnlyMode);
    plugin.saveConfig();

    if (subOnlyMode)
      sender.sendMessage(MCResponse.success("Sub-only mode was enabled."));
    else
      sender.sendMessage(MCResponse.success("Sub-only mode was disabled."));

    return true;
  }

  @Override
  public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, String[] args) {
    ArrayList<String> list = new ArrayList<>();
    list.add("true");
    list.add("false");

    return list;
  }
}
