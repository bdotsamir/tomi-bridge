package dev.akii.tomibridge.minecraft.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.minecraft.CompositeCommand;
import dev.akii.tomibridge.minecraft.MCResponse;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ReloadConfig extends CompositeCommand {

  private final TomiBridge plugin;

  public ReloadConfig(TomiBridge plugin) {
    super("reloadconfig", "Reloads all files in the plugin's folder", "<command>", new ArrayList<>());
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("tomibridge.reloadconfig");
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    // reload *all* the configs.
    plugin.reloadConfig();
    plugin.getWhitelist().reloadConfig();
    plugin.getReapedPlayers().reloadConfig();

    sender.sendMessage(MCResponse.success("Reloaded all configs"));

    return true;
  }
}
