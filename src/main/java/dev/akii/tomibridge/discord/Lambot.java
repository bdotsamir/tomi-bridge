package dev.akii.tomibridge.discord;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.discord.commands.*;
import dev.akii.tomibridge.discord.events.MessageListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import static org.bukkit.Bukkit.getLogger;

public class Lambot {

  private final TomiBridge plugin;
  private JDA api;
  private HashMap<String, DiscordCommandInterface> commands;

  public Lambot(@NotNull TomiBridge plugin) {

    this.plugin = plugin;

    try {
      String token = plugin.getConfig().getString("discord.token");

      this.commands = new HashMap<>();
      commands.put("ping", new Ping());
      commands.put("whitelist", new Whitelist());
      commands.put("reload", new Reload());
      commands.put("resurrect", new Resurrect());
      commands.put("nepotism", new Nepotism());
      commands.put("subonly", new SubOnlyMode());

      MessageListener messageListener = new MessageListener(this.plugin, this, this.commands);

      // export the JDA object
      this.api = JDABuilder.createDefault(token,
                      GatewayIntent.GUILD_MEMBERS,
                      GatewayIntent.GUILD_MESSAGES)
              .addEventListeners(messageListener)
              .build().awaitReady();
    } catch (Exception e) {
      getLogger().severe("Bot could not be initialized: " + e);
    }
  }

  public TomiBridge getPlugin() {
    return this.plugin;
  }

  public JDA getAPI() {
    return this.api;
  }

  public HashMap<String, DiscordCommandInterface> getCommands() {
    return this.commands;
  }
}
