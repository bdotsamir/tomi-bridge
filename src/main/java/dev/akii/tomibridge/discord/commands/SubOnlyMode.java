package dev.akii.tomibridge.discord.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;
import org.jetbrains.annotations.NotNull;

public class SubOnlyMode implements DiscordCommandInterface {

  public void run(TomiBridge plugin, @NotNull Lambot bot, Message message, String[] args) {
    boolean subOnlyMode = plugin.getConfig().getBoolean("subOnlyMode");

    try {
      String enabledDisabled = args[0];
      if (enabledDisabled.equals("true"))
        subOnlyMode = true;
      else if (enabledDisabled.equals("false")) {
        subOnlyMode = false;
      } else {
        message.getChannel().sendMessage(":x: **Must be one of `true` or `false` (or omitted to toggle it).**").queue();
        return;
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      subOnlyMode = !subOnlyMode; // toggle the boolean
    }

    plugin.getConfig().set("subOnlyMode", subOnlyMode);
    plugin.saveConfig();

    if (subOnlyMode)
      message.getChannel().sendMessage(":white_check_mark: **Sub-only mode was enabled.**").queue();
    else
      message.getChannel().sendMessage(":white_check_mark: **Sub-only mode was disabled.**").queue();
  }

}
