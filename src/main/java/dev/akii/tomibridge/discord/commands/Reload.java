package dev.akii.tomibridge.discord.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;

public class Reload implements DiscordCommandInterface {

  public void run(TomiBridge plugin, Lambot bot, Message message, String[] args) {
    plugin.reloadConfig();
    message.getChannel().sendMessage(":white_check_mark: **`config.yml` was reloaded!**").queue();
  }

}
