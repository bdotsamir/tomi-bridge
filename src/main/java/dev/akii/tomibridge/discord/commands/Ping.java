package dev.akii.tomibridge.discord.commands;

import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;
import org.jetbrains.annotations.NotNull;

public class Ping implements DiscordCommandInterface {

  public void run(TomiBridge plugin, @NotNull Lambot bot, Message message, String[] args) {
    bot.getAPI().getRestPing().queue((time) -> message.getChannel().sendMessageFormat(":ping_pong: **Pong!** `%dms`", time).queue());
  }

}
