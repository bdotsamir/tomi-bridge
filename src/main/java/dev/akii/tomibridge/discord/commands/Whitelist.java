package dev.akii.tomibridge.discord.commands;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.Utils;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class Whitelist implements DiscordCommandInterface {

  public void run(TomiBridge plugin, Lambot bot, @NotNull Message message, String[] args) {
    Member member = message.getMember();
    if (member == null) {
      message.getChannel().sendMessage(":warning: **Member was null.** This is likely a caching issue. Try again in a few minutes?").queue();
      return;
    }
    List<Role> roles = member.getRoles();

    String subRoleString = plugin.getConfig().getString("discord.subRoleID");
    if (subRoleString == null || subRoleString.equals("")) {
      message.getChannel().sendMessage(":x: **discord.subRoleID is not set in config.**").queue();
      return;
    }
    Role subRole = bot.getAPI().getRoleById(subRoleString);

    boolean subModeEnabled = plugin.getConfig().getBoolean("subOnlyMode");

    if (!roles.contains(subRole) && !plugin.getDebugMode() && subModeEnabled) {
      message.getChannel().sendMessage(":x: **You are not a subscriber to scrungeonmaster!**").queue();
      return;
    }

    String minecraftUsername;
    try {
      minecraftUsername = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      message.getChannel().sendMessage(":x: **Missing Minecraft username to whitelist.**").queue();
      return;
    }

    plugin.getLogger().info(minecraftUsername);

    String urlToSend = "https://api.mojang.com/users/profiles/minecraft/" + minecraftUsername + "?at=" + System.currentTimeMillis() / 1000L;

    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder(
                    URI.create(urlToSend)
            )
            .header("accept", "application/json")
            .build();

    CompletableFuture<HttpResponse<Supplier<PlayerJSON>>> response = client.sendAsync(request, new Utils.JsonBodyHandler<>(PlayerJSON.class));

    message.getChannel().sendMessage(":gear: **Getting UUID...**").queue(msg -> {
      try {
        HttpResponse<Supplier<PlayerJSON>> res = response.get();

        String discordID = message.getAuthor().getId();
        String UUID = res.body().get().id;
        String formattedUUID = Utils.formatUUID(UUID);

        plugin.getLogger().fine(UUID);
        plugin.getLogger().fine(formattedUUID);

        Utils.CustomConfig whitelist = plugin.getWhitelist();

        String user = whitelist.getConfig().getString(formattedUUID);
        if (user != null) {
          msg.editMessage(":x: **You've already linked your account.**").queue();
          return;
        }

        whitelist.getConfig().addDefault(formattedUUID + ".type", "subscriber");
        whitelist.getConfig().addDefault(formattedUUID + ".discordID", discordID);
        whitelist.saveConfig();

        msg.editMessage(":white_check_mark: **Successfully added `" + minecraftUsername + "` (`" + formattedUUID + "`) to the whitelist.**").queue();
      } catch (Exception e) {
        msg.editMessage(":x: **Sorry! There has been some sort of error.** Please check the username spelling or try again later.").queue();
        e.printStackTrace();
      }
    });

  }

  // This class provides a source for deserialization the JSON we receive from Mojang
  private static class PlayerJSON {
    public final String name;
    public final String id;

    public PlayerJSON(
            @JsonProperty("name") String name,
            @JsonProperty("id") String id
    ) {
      this.name = name;
      this.id = id;
    }
  }

}
