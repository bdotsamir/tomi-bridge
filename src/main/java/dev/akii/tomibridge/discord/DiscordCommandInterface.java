package dev.akii.tomibridge.discord;

import dev.akii.tomibridge.TomiBridge;
import net.dv8tion.jda.api.entities.Message;

public interface DiscordCommandInterface {

  void run(TomiBridge plugin, Lambot bot, Message message, String[] args);

}
