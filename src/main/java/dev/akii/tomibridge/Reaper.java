package dev.akii.tomibridge;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import org.bukkit.BanList;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// This class is responsible for all things life and death on the server and discord.
// When a player dies, the reaper reaps (which bans them for the configured amount and adds the Dead role on discord),
// and when the death timer is up, the reaper resurrects them (pardons the ban and removes the Dead role)
public class Reaper {

  private final TomiBridge plugin;
  private final Guild guild;
  private final Role deathRole;

  public Reaper(@NotNull TomiBridge plugin, @NotNull String guildID, @NotNull String deathRoleID) throws Exception {
    this.plugin = plugin;
    JDA api = plugin.getBot().getAPI();

    this.guild = plugin.getGuild();

    Role deathRole = api.getRoleById(deathRoleID);
    if (deathRole == null)
      throw new Exception("Death role could not be found on Discord");

    this.deathRole = deathRole;
  }

  // shortcut method :)
  public Utils.CustomConfig getConfig() {
    return plugin.getReapedPlayers();
  }

  public void reap(@NotNull Member member, @NotNull Player player) {
    int duration = plugin.getConfig().getInt("deathTimeInMinutes");
    String formattedDuration = Utils.SecondsFormatter.format(duration);

    applyRole(member);
    //plugin.getServer().banIP(IP);
    player.banPlayerIP("You died to PvP! Come back in " + formattedDuration + ".", true);

    InetSocketAddress address = player.getAddress();
    assert address != null;
    String IP = address.getAddress().getHostAddress();
    IP = IP.replaceAll("\\.", "_");

    plugin.getLogger().fine("ip: " + IP);

    LocalDateTime dateNow = LocalDateTime.now();
    LocalDateTime dateInTheFuture = dateNow.plusMinutes(duration);

    plugin.getLogger().fine("date now: " + dateNow);
    plugin.getLogger().fine("date after configured duration: " + dateInTheFuture);

    plugin.getReapedPlayers().getConfig().addDefault(IP + ".discordID", member.getId());
    plugin.getReapedPlayers().getConfig().addDefault(IP + ".until", dateInTheFuture.toString());
    plugin.getReapedPlayers().saveConfig();

    //assert duration != null; // by default it has a value. if it doesn't, the plugin will error. so be it.

    // 4320 (the default config value) is 3 days in minutes
    setTimer(member, IP, duration);

    plugin.getLogger().info("[Reaper] Reaped " + member.getEffectiveName() + " >:)");
  }

  // IP must be in format `x_x_x_x`, not `x.x.x.x`
  public void resurrect(@NotNull Member member, @NotNull String IPwUnderscores) {
    String IPwDots = IPwUnderscores.replaceAll("_", ".");

    plugin.getLogger().fine("IPwUnderscores " + IPwUnderscores);

    removeRole(member);
    BanList banList = plugin.getServer().getBanList(BanList.Type.IP);
    banList.pardon(IPwDots);

    // remove the user from the reaped players list
    plugin.getReapedPlayers().getConfig().set(IPwUnderscores, null);
    plugin.getReapedPlayers().saveConfig();

    plugin.getLogger().info("[Reaper] Resurrected " + member.getEffectiveName() + " :)");
  }

  private void applyRole(@NotNull Member member) {
    // wtf is this JDA
    guild.addRoleToMember(member, deathRole).reason("Player died in-game").queue();
  }

  private void removeRole(@NotNull Member member) {
    guild.removeRoleFromMember(member, deathRole).reason("Death timer expired").queue();
  }

  // IP must be in the format `x_x_x_x`, not `x.x.x.x`
  public void setTimer(@NotNull Member member, @NotNull String IP, int duration) {
    plugin.getLogger().fine("Sleeping for " + duration + " minutes!");
    ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);

    // sleep a new thread for three days

    executor.schedule(new Runnable() {
      @Override
      public void run() {
        plugin.getLogger().fine("Done sleeping! Good morning :)");

        // then run the resurrect function
        resurrect(member, IP);
      }
    }, duration, TimeUnit.MINUTES);
  }

}
