# Tomi Bridge
Connects the minecraft server to tomi's discord server 

[![Build Status](http://ci.akii.dev/buildStatus/icon?job=tomi-bridge)]()

## How to
1. Grab the [latest successful build](https://ci.akii.dev/job/tomi-bridge/lastSuccessfulBuild/artifact/target/tomi-bridge.jar) from the jenkins server
2. Drop it in the `plugins/` folder of your **paper** minecraft server. (it must be paper. (I think. (if you aren't using paper, why?)))
3. Run the server once to let the plugin generate all the necessary config files
4. Stop the server
5. Edit the plugin config in `plugins/TomiBridge/config.yml` and fill in all the necessary values
6. Start the server!

**You don't need to have whitelist enabled in order for this plugin to work.** In fact, if you do have it enabled, the plugin warns you to disable it. 
To link each subscriber to their minecraft username, have them run `>whitelist <mc username>` in Discord. If they mess up, just go into the **plugin's** `whitelist.yml` (**not the server's**) and delete the entry.

## Dev to-do:
<details>
  <summary>Finished tasks</summary>

* [x] Create a config
* [x] Detect when a player dies on the server
  * [x] Detect when a player dies to PVP on the server
    * [org.bukkit.event.entity.EntityDeathEvent.getEntity().getKiller()](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/LivingEntity.html#getKiller())
    * also look into the [explode event](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/event/entity/EntityExplodeEvent.html)?
* [x] Have users run a command on discord (`!link <mc username>`) and store that data for later retrieval
* [x] Ban for three days on mc server & mute for three days on discord server
* [x] Persist 3-day-timer through restarts of the server
  * [x] Create persistent file (CustomConfig reapedplayers.yml)
  * [x] Read `until` property of reaped IP and set ScheduledThreadPoolExecutor to remaining time (likely in `TimeUnit.SECONDS`)<br>
    Use the `Reaper#setTimer()` method.
* [x] Add check for already-expired reaped players on server startup and resurrect them accordingly
* [x] Delete users from `reapedplayers.yml` when their time is up
* [x] Keep users on discord from whitelisting themselves more than once
* [x] Add some more commands discord-side and server-side to configure the whitelist and death timers
  * [x] `>resurrect <ip>`
  * [x] `/resurrect <ip/online player>`
  * [x] `/setlogger <level>` - sets the plugin's logger's verbosity level
* [x] Reloadable config
  * [x] `/reload`
  * [x] `>reload`
* [x] Create some pretty minecraft logging thing so it isn't just white text.
  * `[TomiBridge] &cThat action is not allowed!`
  * `[TomiBridge] &aUpdated successfully`
* [x] Dynamic minecraft command registering https://www.spigotmc.org/threads/is-there-any-way-to-write-to-the-plugin-yml-file-with-code.362432/
</details>

* [ ] TODO: this https://www.spigotmc.org/wiki/stop-tabs-from-resetting-your-config-files/
* [ ] Create super-command on minecraft that houses all the other commands
  * `/tomi reload`
  * `/tomi setlogger`
  * `/tomi xyz`
* [ ] More minecraft perms the higher the subscriber tier
* [ ] Dynamic config editor
  * `>config discord.subRoleID` replies with the subscriber role ID
  * `>config discord.subRoleID 713692914559090688` edits the role ID
  * and copy this over to a minecraft command
* [ ] Resurrect minecraft command displays usernames instead of IPs
* [ ] (low priority) create small utility function that formats seconds into human-readable time
  * 100 seconds => "2 minutes" (1.666... rounded up)
  * 3600 seconds => "1 hour"
  * 1200 seconds => "1 day"
  * and so on